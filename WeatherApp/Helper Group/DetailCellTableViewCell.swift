//
//  DetailCellTableViewCell.swift
//  WeatherApp
//
//  Created by Eshan Cheema on 10/7/21.
//

import UIKit

class DetailCellTableViewCell: UITableViewCell {
    @IBOutlet weak var lblHeight: NSLayoutConstraint!
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var imgWeather: UIImageView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblTypeWeather: UILabel!
    @IBOutlet weak var lblTemperature: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
