//
//  ViewController.swift
//  WeatherApp
//
//  Created by Eshan Cheema on 10/7/21.
//

import UIKit
import GoogleMaps
import GooglePlaces
import PromiseKit
import JGProgressHUD
import Alamofire
import SwiftyJSON

class ViewController: UIViewController {
    
    //  Mark : Outlets
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var imgWeather: UIImageView!
    @IBOutlet weak var lblWeatherType: UILabel!
    @IBOutlet weak var lblRain: UILabel!
    @IBOutlet weak var lblRainMeasure: UILabel!
    @IBOutlet weak var lblTemperature: UILabel!
    @IBOutlet weak var lblWind: UILabel!
    @IBOutlet weak var lblWindDirection: UILabel!
    
    var strWeatherDetail = String()
    var strTemperature = String()
    let activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        LocationManager.shared.requestLocationAtOnce()
        let location = CLLocation(latitude: LocationManager.shared.currentLocation.coordinate.latitude, longitude: LocationManager.shared.currentLocation.coordinate.longitude)
        LocationManager.shared.fetchAddress(location: location) { (address, formatted, error, city) in
            self.getAddressFromLatLon(pdblLatitude: LocationManager.shared.currentLocation.coordinate.latitude, withLongitude: LocationManager.shared.currentLocation.coordinate.longitude)
        }
    }
    
    // Mark : Button action
    @IBAction func actionShare(_ sender: UIButton) {
        share()
    }
    
    // Mark : Get Address from latitude and logitude
    func getAddressFromLatLon(pdblLatitude: Double, withLongitude pdblLongitude: Double) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        ceo.reverseGeocodeLocation(loc, completionHandler:
                                    {(placemarks, error) in
                                        if (error != nil)
                                        {
                                            print("reverse geodcode fail: \(error!.localizedDescription)")
                                        }
                                        let pm = placemarks! as [CLPlacemark]
                                        if pm.count > 0 {
                                            let pm = placemarks![0]
                                            var addressString : String = ""
                                            if pm.subLocality != nil {
                                                addressString = addressString + pm.subLocality! + ", "
                                            }
                                            if pm.thoroughfare != nil {
                                                addressString = addressString + pm.thoroughfare! + ", "
                                            }
                                            if pm.locality != nil {
                                                addressString = addressString + pm.locality! + ", "
                                            }
                                            if pm.country != nil {
                                                addressString = addressString + pm.country! + ", "
                                            }
                                            if pm.postalCode != nil {
                                                addressString = addressString + pm.postalCode! + " "
                                            }
                                            
                                            print(addressString)
                                            self.WindApi(location: pm.locality ?? "")
                                        }
                                    })
        
    }
    
    // Mark : Share Data
    func share() {
        if let name = NSURL(string: "Hey friends,\n Today weather will be\(strWeatherDetail + "and" + "temperature will be" + strTemperature)") {
            let message = MessageWithSubject(subject: "Weather data", message: "\(name)")
            let objectsToShare = [message]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            self.present(activityVC, animated: true, completion: nil)
        } else {
            let alertController = UIAlertController(title: "No data available...", message: "You can't provide the inforamtion to near one at that movement.", preferredStyle: UIAlertController.Style.alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                (result : UIAlertAction) -> Void in
                print("OK")
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
}


//MARK:- ----Wind Api Handler Extension----
extension ViewController {
    private func WindApi(location : String){
        self.startLoading()
        APIManager.LoactionApi(latitude: LocationManager.shared.currentLocation.coordinate.latitude , longitude: LocationManager.shared.currentLocation.coordinate.longitude , City: location, viewController: self)
        {(json) in
            if json["cod"].stringValue == "200" {
                let dictCity = json["city"].dictionaryValue
                let location = "\(dictCity["name"]!.stringValue) , \(dictCity["country"]!.stringValue)"
                self.lblLocation.text = location
                let arrlist = json["list"].arrayValue
                let arrWeather = arrlist[0]["weather"].arrayValue
                let icon = arrWeather[0]["icon"].dictionaryValue
                let imgUrl = "http://openweathermap.org/img/w/"
                let imgData = arrWeather[0]["icon"].stringValue
                let imgExtension = ".png"
                let location1 = "\(imgUrl)\(imgData)\(imgExtension)"
                let url = URL(string: location1)
                let data = try? Data(contentsOf: url!)
                if let imageData = data {
                    let image = UIImage(data: imageData)
                    self.imgWeather.image = image
                }
                let temp = arrlist[0]["main"]["temp"].doubleValue - 273.0
                let measurement = Measurement(value: temp, unit: UnitTemperature.celsius)
                let measurementFormatter = MeasurementFormatter()
                measurementFormatter.unitStyle = .short
                measurementFormatter.numberFormatter.maximumFractionDigits = 0
                measurementFormatter.unitOptions = .temperatureWithoutUnit
                self.lblWeatherType.text = measurementFormatter.string(from: measurement) + "C" + "/" + arrWeather[0]["description"].stringValue
                self.lblWind.text = arrlist[0]["wind"]["speed"].stringValue + " km/h"
                self.updateDirectionTextFromHeading(magneticHeading:arrlist[0]["wind"]["327"].doubleValue )
                self.stopLoading()
            }
            
            else{
                
                print(json)
                
            }
            
        }
    }
    private func updateDirectionTextFromHeading(magneticHeading:Double) {
        let gradToRotate = Int(magneticHeading)
        var strDirection = ""
        if(gradToRotate > 23 && gradToRotate <= 67){
            strDirection = "NE";
        } else if(gradToRotate > 68 && gradToRotate <= 112){
            strDirection = "E";
        } else if(gradToRotate > 113 && gradToRotate <= 167){
            strDirection = "SE";
        } else if(gradToRotate > 168 && gradToRotate <= 202){
            strDirection = "S";
        } else if(gradToRotate > 203 && gradToRotate <= 247){
            strDirection = "SW";
        } else if(gradToRotate > 248 && gradToRotate <= 293){
            strDirection = "West";
        } else if(gradToRotate > 294 && gradToRotate <= 337){
            strDirection = "NW"
        } else if(gradToRotate >= 338 || gradToRotate <= 22){
            strDirection = "N"
        }
        
        self.lblWindDirection.text = strDirection
        print("\(strDirection)")
    }
    
}





