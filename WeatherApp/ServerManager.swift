//
//  ServerManager.swift
//  DanHotel
//
//  Created by Zoptal on 10/02/21.
//

import UIKit
import CFNetwork
import Alamofire
import SwiftyJSON
import MobileCoreServices

typealias ServerSuccessCallBack = (_ json:JSON, _ statusCode:Int)->Void
typealias SocketComplitionBlock = (_ json:Any)->Void
typealias ServerFailureCallBack=(_ error:Error?)->Void
typealias ServerProgressCallBack = (_ progress:Double?) -> Void
typealias ServerNetworkConnectionCallBck = (_ reachable:Bool) -> Void
var window: UIWindow?
class ServerManager: NSObject {
    
    override init() {
        super.init()
    }
    //MARK: - Singlton -
    class var shared:ServerManager{
        struct  Singlton {
            static let instance = ServerManager()
        }
        return Singlton.instance
    }

    
    //MARK: - Session Manager -
    private lazy var Session: Alamofire.Session = {
        let configure  = URLSessionConfiguration.default
        configure.timeoutIntervalForRequest = 30
        return Alamofire.Session(configuration: configure)
    }()
    
    //MARK:- documentsDirectoryURL -
    lazy var documentsDirectoryURL : URL = {
        let documents = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        return documents
    }()
    
    //MARK: - Check Network Connetion -
   
    

    
    //MARK: - Header -
  
    
//    func m_AppHeader() ->  HTTPHeaders {
//    let header:HTTPHeaders = ["x-logintoken": AppManager.getLoggedInUser().loggedInAccessToken]
//     return header
//    }
    
    func m_AppHeader() ->  HTTPHeaders {
    let header:HTTPHeaders = ["x-logintoken": ""]
     return header
    }

    //MARK:- Post -
    func Post(request url: String!,params: [String:Any]!,successHandler:ServerSuccessCallBack?,failureHandler:ServerFailureCallBack?){
        AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: m_AppHeader()).response { (response) in
            switch(response.result) {
            case .success(_):
                do {
                    let json = try JSON(data: response.data!)
                    if (successHandler != nil){
                        if ((json.null) == nil){
                            if json["code"].intValue == 203 && json["result"].boolValue == false && json["message"].stringValue == "Session Expired, Please login again" {
                                
                            }else{
                                successHandler!(json, (response.response?.statusCode)!)
                            }
                        }
                    }
                } catch {
                    print("error")
                }
                break
            case .failure(let error):
                if (failureHandler != nil){
                    failureHandler!(error)
                }
                break
            }
        }
    }
    
    
    
    //MARK:- Post -
    func Put(request url: String!,params: [String:Any]!,successHandler:ServerSuccessCallBack?,failureHandler:ServerFailureCallBack?){
     
        AF.request(url, method: .put, parameters: params, encoding: URLEncoding.default, headers: m_AppHeader()).response { (response) in
            switch(response.result) {
            case .success(_):
                do {
                    let json = try JSON(data: response.data!)
                    if (successHandler != nil){
                        if ((json.null) == nil){
                            if json["code"].intValue == 203 && json["result"].boolValue == false && json["message"].stringValue == "Session Expired, Please login again" {
                               
                            }else{
                                successHandler!(json, (response.response?.statusCode)!)
                            }
                        }
                    }
                } catch {
                    print("error")
                }
                break
            case .failure(let error):
                if (failureHandler != nil){
                    failureHandler!(error)
                }
                break
            }
        }
    }
    
    //MARK:- Post -
    func Put1(request url: String!,params: [String:Any]!,successHandler:ServerSuccessCallBack?,failureHandler:ServerFailureCallBack?){
     
        AF.request(url, method: .put, parameters: params, encoding: JSONEncoding.default, headers: m_AppHeader()).response { (response) in
            switch(response.result) {
            case .success(_):
                do {
                    let json = try JSON(data: response.data!)
                    if (successHandler != nil){
                        if ((json.null) == nil){
                            if json["code"].intValue == 203 && json["result"].boolValue == false && json["message"].stringValue == "Session Expired, Please login again" {
                                
                            }else{
                                successHandler!(json, (response.response?.statusCode)!)
                            }
                        }
                    }
                } catch {
                    print("error")
                }
                break
            case .failure(let error):
                if (failureHandler != nil){
                    failureHandler!(error)
                }
                break
            }
        }
    }
    
    func Post1(request url: String!,params: [String:Any]!,successHandler:ServerSuccessCallBack?,failureHandler:ServerFailureCallBack?){
        AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: m_AppHeader()).response { (response) in
               switch(response.result) {
               case .success(_):
                   do {
                       let json = try JSON(data: response.data!)
                       if (successHandler != nil){
                           if ((json.null) == nil){
                               if json["code"].intValue == 203 && json["result"].boolValue == false && json["message"].stringValue == "Session Expired, Please login again" {
                                   
                               }else{
                                   successHandler!(json, (response.response?.statusCode)!)
                               }
                           }
                       }
                   } catch {
                       print("error")
                   }
                   break
               case .failure(let error):
                   if (failureHandler != nil){
                       failureHandler!(error)
                   }
                   break
               }
           }
       }

    //MARK:- Delete -
    func Delete(request url: String!,params: [String:Any]!,appHeader:[String:String]? = nil,successHandler:ServerSuccessCallBack?,failureHandler:ServerFailureCallBack?){
        AF.request(url, method: .delete, parameters: params, encoding: URLEncoding.default, headers: m_AppHeader()).response { (response) in
            switch(response.result) {
            case .success(_):
                do {
                    let json = try JSON(data: response.data!)
                    if (successHandler != nil) {
                        if ((json.null) == nil) {
                            successHandler!(json, (response.response?.statusCode)!)
                        }
                    }
                } catch {
                    print("error")
                }
                break
            case .failure(let error):
                if (failureHandler != nil){
                    failureHandler!(error)
                }
                break
            }
        }
    }

    //MARK:- Get -
    func get(url:String!,params: [String:Any]!,successHandler:ServerSuccessCallBack?, failureHandler:ServerFailureCallBack?) {
        AF.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil).response { (response) in
            switch(response.result) {
            case .success(_):
                do {
                    let json = try JSON(data: response.data!)
                    if (successHandler != nil){
                        if ((json.null) == nil){
                            successHandler!(json, (response.response?.statusCode)!)
                        }
                    }
                } catch {
                    print("error")
                }
                break
            case .failure(let error):
                if (failureHandler != nil){
                    failureHandler!(error)
                }
                break
            }
        }
    }
    
    //MARK:- Get -
    func get1(url:String!,params: [String:Any]!,successHandler:ServerSuccessCallBack?, failureHandler:ServerFailureCallBack?) {
        print(m_AppHeader)
        AF.request(url, method: .get, parameters: params, encoding: JSONEncoding.default, headers: m_AppHeader()).response { (response) in
            switch(response.result) {
            case .success(_):
                do {
                    let json = try JSON(data: response.data!)
                    if (successHandler != nil){
                        if ((json.null) == nil){
                            successHandler!(json, (response.response?.statusCode)!)
                        }
                    }
                } catch {
                    print("error")
                }
                break
            case .failure(let error):
                if (failureHandler != nil){
                    failureHandler!(error)
                }
                break
            }
        }
    }

    //MARK:- UPLOAD MULTIPLE -
    func httpUpload(api:String! ,params:[String:Any]?, multipartObject:[MultipartData]?,successHandler:ServerSuccessCallBack?,failureHandler:ServerFailureCallBack?,progressHandler:ServerProgressCallBack?){
        if (multipartObject != nil) {
            AF.upload(multipartFormData: { multipartFormData in
                if let mediaList  = multipartObject {
                    for object in mediaList{
                        multipartFormData.append(object.media, withName: object.mediaUploadKey, fileName: object.fileName, mimeType: object.mimType)
                    }
                }
                if (params != nil) {
                    for (key, value) in params! {
                        multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
                    }
                }
            },to: api, method: .post , headers: m_AppHeader()).response { (response) in
                switch(response.result) {
                case .success(_):
                    do {
                        let json = try JSON(data: response.data!)
                        if (successHandler != nil) {
                            if ((json.null) == nil) {
                                if json["code"].intValue == 203 && json["result"].boolValue == false && json["message"].stringValue == "Session Expired, Please login again" {
                                  
                                }else{
                                    successHandler!(json, (response.response?.statusCode)!)
                                }
                            }
                        }
                    } catch { print("error")}
                    break
                case .failure(let error):
                    if (failureHandler != nil) {
                        failureHandler!(error)
                    }
                    break
                }
            }
        }else{
            self.Post(request: api, params: params, successHandler: successHandler, failureHandler: failureHandler)
        }
    }
    
    
    
    //MARK:- Download -
    func httpDownload(request api:String! ,enableBackGroundTask isBackground:Bool,successHandler:@escaping (_ result:Any)->Void?,failureHandler:ServerFailureCallBack?,progressHandler:ServerProgressCallBack?){
        let fileUrl  = URL(string: api)
        let request = URLRequest(url: fileUrl!)
        let destination: DownloadRequest.Destination = { filePath,response in
            let directory : NSURL = (self.documentsDirectoryURL as NSURL)
            let fileURL = directory.appendingPathComponent(response.suggestedFilename!)!
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }

            AF.download(request, to: destination).response { (response) in
                switch(response.result){
                case .success(_):
                    successHandler(response.fileURL!)
                    break
                case .failure(let error):
                    if (failureHandler != nil){
                        failureHandler!(error)
                    }
                    break
                }
                }.downloadProgress { (progress) in
                    if (progressHandler != nil){
                        progressHandler!(progress.fractionCompleted)
                    }
            }

    }

  
  
    
   
}

//MARK:- AppUtility -
class AppUtility:NSObject{
    class func setGradientColor(_ colors:[Any],view:UIView){
        let gradient = CAGradientLayer()
        gradient.frame = view.bounds
        gradient.colors = colors
        gradient.locations = [0.0, 0.25, 0.75, 1.0]
        view.layer.addSublayer(gradient)
    }
    //MARK:- mimeTypeForPath -
    class func mimeType(forPath filePath:URL)->String{
        var  mimeType:String;
        let fileExtension:CFString = filePath.pathExtension as CFString
        let UTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, fileExtension, nil);
        let str = UTTypeCopyPreferredTagWithClass(UTI!.takeUnretainedValue(), kUTTagClassMIMEType);
        if (str == nil) {
            mimeType = "application/octet-stream";
        } else {
            mimeType = str!.takeUnretainedValue() as String
        }
        return mimeType
    }
    //MARK:- filename -
    class func filename(Prefix:String , fileExtension:String)-> String{
        let dateformatter=DateFormatter()
        dateformatter.dateFormat="MddyyHHmmss"
        let dateInStringFormated=dateformatter.string(from: Date() )
        return "\(Prefix)_\(dateInStringFormated).\(fileExtension)"
    }
    
    //MARK:-get JsonObjectDict -
    class func getJsonObjectDict(responseObject:Any)->[String:Any]{
        var anyObj :[String:Any]!
        do{
            anyObj = try (JSONSerialization.jsonObject(with: (responseObject as! NSData) as Data, options: []) as? [String:Any])
            return anyObj!
        } catch  {
        }
        return anyObj!
    }
    
    //MARK:- getJsonObjectArray -
    class func JSONArray(responseObject:Any)->[Any]{
        var anyObj :[Any] = [Any]()
        do{
            anyObj = try JSONSerialization.jsonObject(with: (responseObject as! NSData) as Data, options: []) as! [Any]
            return anyObj
            
        } catch  {
        }
        return anyObj
    }
    
    //MARK:- JSONStringify -
    class func JSONStringify(value: AnyObject, prettyPrinted: Bool = false) -> String{
        let options = prettyPrinted ? JSONSerialization.WritingOptions.prettyPrinted : JSONSerialization.WritingOptions(rawValue: 0)
        if JSONSerialization.isValidJSONObject(value){
            if let data = try? JSONSerialization.data(withJSONObject: value, options: options){
                if let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue){
                    return string as String
                }
            }
        }
        return ""
    }
    
   
    
    //MARK:- getJsonData -
    class func JSONData(responseObject:Any)->Data{
        var data :Data!
        do{
            data = try JSONSerialization.data(withJSONObject: responseObject, options: []) as Data
            return data!
            
        } catch  {
        }
        return data!
    }
  
}

//MARK: - Multipart Object Class -
class MultipartData: NSObject {
    var media:Data!
    var mediaUploadKey:String!
    var fileName:String!
    var mimType:String!
    
    override init(){ }
    
    init(medaiObject object:Any!,mediaKey uploadKey: String!,isPNG: Bool = true ,fileName : String) {
        if (object != nil) , (uploadKey != nil) {
            if  object is UIImage {
                if let imageObject = object as? UIImage {
                    self.media =  imageObject.pngData() /*isPNG == true ? imageObject.uncompressedPNGData : imageObject.lowQualityJPEGNSData*/
                    self.mimType = "image/png"
                    self.fileName = fileName
                }
            }else{
                if  let filepath = object as? String{
                    let url = NSURL.fileURL(withPath: filepath)
                    self.fileName = url.lastPathComponent
                    self.media = try! Data(contentsOf: url) //NSData(contentsOf: url)
                    self.mimType = AppUtility.mimeType(forPath: url )
                }
            }
            self.mediaUploadKey = uploadKey
        }
    }
    
    required init(coder aDecoder: NSCoder) {
        
        if let media = aDecoder.decodeObject(forKey: "mediaData") as? Data {
            self.media = media
        }
        
        if let mediaUploadKey = aDecoder.decodeObject(forKey: "mediaUploadKey") as? String {
            self.mediaUploadKey = mediaUploadKey
        }
        
        if let fileName = aDecoder.decodeObject(forKey: "fileName") as? String {
            self.fileName = fileName
        }
        
        if let mimType = aDecoder.decodeObject(forKey: "mimType") as? String {
            self.mimType = mimType
        }
    }
    open func encodeWithCoder(_ aCoder: NSCoder){
        
        if let media = self.media{
            aCoder.encode(media, forKey: "media")
        }
        
        if let mediaUploadKey = self.mediaUploadKey {
            aCoder.encode(mediaUploadKey, forKey: "mediaUploadKey")
        }
        
        if let fileName = self.fileName {
            aCoder.encode(fileName, forKey: "fileName")
        }
        
        if let mimType = self.mimType {
            aCoder.encode(mimType, forKey: "mimType")
        }
    }
}

