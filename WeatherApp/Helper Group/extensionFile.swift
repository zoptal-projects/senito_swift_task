//
//  extensionFile.swift
//  WeatherApp
//
//  Created by Eshan Cheema on 10/9/21.
//

import Foundation
import UIKit

let activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()

extension UIViewController {

func startLoading() {
    activityIndicator.center = self.view.center
    activityIndicator.hidesWhenStopped = true
    activityIndicator.style = UIActivityIndicatorView.Style.large
    activityIndicator.color = .darkGray
    DispatchQueue.main.async {
        self.view.addSubview(activityIndicator)
    }
    activityIndicator.startAnimating()
}

func stopLoading() {
    activityIndicator.stopAnimating()
    activityIndicator.removeFromSuperview()
    activityIndicator.isHidden = true
  }
}

