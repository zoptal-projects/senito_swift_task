//
//  Configuration.swift
//  WeatherTron
//
//  Created by Bhaskaran, Suman (US - Bengaluru) on 12/23/17.
//  Copyright © 2017 Wipro. All rights reserved.
//

import Foundation

struct Config {
    static let APIKey = "9dde81fe6075e38b6c5d26bad4c12fdb"
    static let BaseURL = "https://api.openweathermap.org/data/2.5/forecast?"
}
