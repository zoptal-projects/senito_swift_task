//
//  Constants.swift
//  UpDog
//
//  Created by Zoptal Mac Mini on 10/12/20.
//  Copyright © 2020 Zoptal Mac Mini. All rights reserved.
//

import Foundation
import UIKit
import Alamofire


let SCENEDELEGATE = SceneDelegate.sharedDelegate

enum Alerts {
    static let logout = "Are you sure, you want to logout?"
    static let deactivate = "Are you sure, you want to deactivate account?"
    static let no = "No"
    static let yes = "Yes"
    static let cancel = "Cancel"
    static let ok = "Ok"
    static let warning = "Warning"
    static let doNotCamera = "You don't have camera"
    static let doNotGallery = "You don't have gallery"
    static let chooseOption = "Choose option to upload image"
    static let camera = "Camera"
    static let gallery = "Gallery"
    static let notImplemented = "Not implemented yet!"
    static let acceptTerms = "Please agree to our terms & conditions"
    static let enterOtp = "Please enter otp"
    static let otpSent = "Otp sent successfully"
    static let verificationSuccess = "OTP verified successfully"
    static let sessionExpire = "Session Expired"
    static let weAreSorry = "We’re Sorry"
    static let noDataFound = "No more data found 😕"
    static let tryAgain = "Try again?"
    static let nothingToShow = "Nothing to show 😕"
    static let logoutSuccess = "Logged out successfully"
}
enum TitleValue {
    static let enableLocation = "Enable Location"
    static let permissionSettings = "The location permission was not authorized. Please enable it in Settings to continue."
    static let settings = "Settings"
    static let cancel = "Cancel"
    static let aboutUs = "About us"
    static let privacyPolicy = "Privacy Policy"
    static let licenese = "Licenese"
    static let SafetyTips = "Safety Tips"
    static let communityGuidelines = "Community Guidelines"
    static let termsCondition = "Terms & Conditions"
    static let newMatches = "New Matches"
    static let messages = "Messages"
    static let likes = "Likes"
    static let TopPicks = "Top Picks"
    static let superLikes = "Super Likes"
    static let photos = "Photos"
    static let spotify = "Spotify"
    static let school = "School"
    static let bio = "Bio"
    static let work = "Work"
    static let man = "Man"
    static let woman = "Woman"
    static let men = "Men"
    static let women = "Women"
    static let everyone = "Everyone"
    static let Other = "Other"
    static let wifiMobile = "On Wi-Fi And Mobile Data"
    static let wifi = "On Wi-Fi Only"
    static let neverAuto = "Never Auto Play Video"
    static let fromSignup = "From Signup"
    static let fromSignin = "From Signin"
    static let fromEmptyView = ""
}
