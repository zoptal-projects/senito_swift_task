//
//  ForcastVC.swift
//  WeatherApp
//
//  Created by Eshan Cheema on 10/8/21.
//

import UIKit
import SwiftyJSON
import GoogleMaps
import GooglePlaces

class ForcastVC: UIViewController {
    
    //  Mark : Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    var arrJsonData = [JSON]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.tableFooterView = UIView()
        LocationManager.shared.requestLocationAtOnce()
        let location = CLLocation(latitude: LocationManager.shared.currentLocation.coordinate.latitude, longitude: LocationManager.shared.currentLocation.coordinate.longitude)
        LocationManager.shared.fetchAddress(location: location) { (address, formatted, error, city) in
            self.getAddressFromLatLon(pdblLatitude: LocationManager.shared.currentLocation.coordinate.latitude, withLongitude: LocationManager.shared.currentLocation.coordinate.longitude)
        }
    }
    
    // Mark : Get Address from latitude and logitude
    func getAddressFromLatLon(pdblLatitude: Double, withLongitude pdblLongitude: Double) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        ceo.reverseGeocodeLocation(loc, completionHandler:
                                    {(placemarks, error) in
                                        if (error != nil)
                                        {
                                            print("reverse geodcode fail: \(error!.localizedDescription)")
                                        }
                                        let pm = placemarks! as [CLPlacemark]
                                        if pm.count > 0 {
                                            let pm = placemarks![0]
                                            var addressString : String = ""
                                            if pm.subLocality != nil {
                                                addressString = addressString + pm.subLocality! + ", "
                                            }
                                            if pm.thoroughfare != nil {
                                                addressString = addressString + pm.thoroughfare! + ", "
                                            }
                                            if pm.locality != nil {
                                                addressString = addressString + pm.locality! + ", "
                                            }
                                            if pm.country != nil {
                                                addressString = addressString + pm.country! + ", "
                                            }
                                            if pm.postalCode != nil {
                                                addressString = addressString + pm.postalCode! + " "
                                            }
                                            
                                            print(addressString)
                                            self.WindApi(location: pm.locality ?? "")
                                        }
                                    })
        
    }
    
    
}

// Mark : tableView Delegate and data source
extension ForcastVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! DetailCellTableViewCell
        if indexPath.row == 0{
            cell.lblDay.text = "  TODAY"
            cell.lblHeight.constant = 40
        }
        else if indexPath.row == 4{
            cell.lblDay.text = "  TOMARROW"
            cell.lblHeight.constant = 40
        }
        else{
            cell.lblHeight.constant = 0
        }
        let relativeDateFormatter = DateFormatter()
        relativeDateFormatter.timeStyle = .none
        relativeDateFormatter.dateStyle = .medium
        relativeDateFormatter.locale = Locale(identifier: "en_GB")
        relativeDateFormatter.doesRelativeDateFormatting = true
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let myString: String = arrJsonData[indexPath.row]["dt_txt"].stringValue
        
        let myStringArr = myString.components(separatedBy: " ")
        cell.lblTime.text = myStringArr[1]
        let arrWeather = arrJsonData[indexPath.row]["weather"].arrayValue
        let icon = arrWeather[0]["icon"].dictionaryValue
        let imgUrl = "http://openweathermap.org/img/w/"
        let imgData = arrWeather[0]["icon"].stringValue
        let imgExtension = ".png"
        let location1 = "\(imgUrl)\(imgData)\(imgExtension)"
        let url = URL(string: location1)
        let data = try? Data(contentsOf: url!)
        if let imageData = data {
            let image = UIImage(data: imageData)
            cell.imgWeather.image = image
        }
        let temp = arrJsonData[indexPath.row]["main"]["temp"].doubleValue - 273.0
        let measurement = Measurement(value: temp, unit: UnitTemperature.celsius)
        let measurementFormatter = MeasurementFormatter()
        measurementFormatter.unitStyle = .short
        measurementFormatter.numberFormatter.maximumFractionDigits = 0
        measurementFormatter.unitOptions = .temperatureWithoutUnit
        cell.lblTemperature.text = measurementFormatter.string(from: measurement) + "C"
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0{
            return 130
        }
        if indexPath.row == 4{
            return 130
        }
        else{
            return 100
        }
    }
}

//MARK:- ----Wind Api Handler Extension----
extension ForcastVC {
    private func WindApi(location : String){
        self.startLoading()
        APIManager.LoactionApi(latitude: LocationManager.shared.currentLocation.coordinate.latitude , longitude: LocationManager.shared.currentLocation.coordinate.longitude , City: location, viewController: self)
        {(json) in
            if json["cod"].stringValue == "200" {
                let dictCity = json["city"].dictionaryValue
                let location = "\(dictCity["name"]!.stringValue)"
                self.lblTitle.text = location
                for i in json["list"].arrayValue {
                    self.arrJsonData.append(i)
                }
                self.tableView.delegate = self
                self.tableView.dataSource = self
                self.tableView.reloadData()
                self.stopLoading()
            }
            
            else{
                print(json)
            }
            
        }
    }
}
