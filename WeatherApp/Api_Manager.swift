//
//  Api_Manager.swift
//  WeatherApp
//
//  Created by Eshan Cheema on 10/9/21.
//

import Foundation
import SwiftyJSON

class APIManager {
    
    // Mark : Notification Get
    class func LoactionApi(latitude: Double, longitude: Double,City : String , viewController:UIViewController, completion:@escaping(JSON) -> Void){
      
      let urlString =  "\(Config.BaseURL)q=\("Mohali")&lat=\(latitude)&lon=\(longitude)" +
                   "&appid=\(Config.APIKey)"
        
        ServerManager.shared.get(url: urlString, params: [:]) { (JSON, status) in
            print(JSON)
            if JSON["cod"].intValue == 200 {
                completion(JSON)
            }
            else{
               print(JSON)
            }
        } failureHandler: { (_error) in
           
            print(_error!.localizedDescription)
          
        }
    }
    
    
}
